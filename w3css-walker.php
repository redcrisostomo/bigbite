<?php
/**
 * @package W3CSS Walker Nav Menu
 * This is an extension of Walker Nav Menu which aims to facilitate the use of w3css navigation.
 * It adds the classes and data toggles to the parent and child menu ul's and li's.
 * @author Frederick S. Crisostomo
*/
class W3CSS_Walker_Nav_Menu extends Walker_Nav_Menu {
  
    function display_element( $element, &$children_elements, $max_depth=4, $depth=0, $args, &$output )
    {
        $id_field = $this->db_fields['id'];
        if ( is_object( $args[0] ) ) {
            $args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
        }
        return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
    }

    // add classes to ul sub-menus
    function start_lvl( &$output, $depth, $args) {
        $indent = str_repeat( "\t", $depth );
        $output	   .= "\n$indent<ul class=\"sub-menu w3-dropdown-content w3-animate-opacity w3-card-4\">\n";
    }

    // add main/sub classes to li's and links
     function start_el( &$output, $item, $depth, $args ) {
    $class_names = '';

    $classes = empty( $item->classes ) ? array() : (array) $item->classes;

    $classes[] = ($item->current) ? 'active' : '';

    $classes[] = 'menu-item-' . $item->ID;
    
    if ($args->has_children && $depth > 0) {

        $classes[] = ' w3-dropdown-submenu';

    } else if($args->has_children && $depth === 0) {

        $classes[] = ' w3-dropdown-hover';

    }
    $class_names = join(' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );

    
    
    $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

    $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );

    $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

    $output .= $indent . '<li' . $id . $value . $class_names .'>';

    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';

    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';

    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';

    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

    $attributes .= ($args->has_children) 	? ' data-target="#" ' : '';

    $item_output = $args->before;
    
    $item_output .= '<a class="';
    
    $item_output .= ($depth > 0) 	 ? 'w3-theme-d1 w3-hover-text-black"' : 'w3-theme-d1 w3-hover-text-black w3-hover-theme-d1 w3-padding-24"';
    
    $item_output .= $attributes .'>';

    $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;

    $item_output .= ($args->has_children && $depth == 0) ? ' <i class="fa fa-caret-down"></i></a>' : '</a>';

    $item_output .= $args->after;

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}
?>