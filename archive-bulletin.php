<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Socialight
 */

get_header(); ?>

    <main class="main">
        <header class="entry-header w3-theme w3-padding-24">
            <div class="w3-content w3-container">
            <?php 
                if( have_posts() ) {
                    echo '<h1 class="section-header w3-col m7"><span class="w3-text-grey w3-border-bottom w3-border-white">'.str_replace('Archives:', '', get_the_archive_title()).'</h1>';
                    the_archive_description( '<div class="w3-text-grey w3-col m7">', '</div>' );
                }
            ?>
            </div>
        </header><!-- .entry-header -->
        <div class="w3-content">
            <div class="w3-row">
                <div class="w3-col <?php echo ( ! is_active_sidebar( 'sidebar-1' ) && ! is_active_sidebar( 'sidebar-2' ) ) ? 'm12 s12' : 'm9 s12' ?>">
                        <?php
                        if ( have_posts() ) : ?>
                                <?php
                                $i=1;
                                /* Start the Loop */
                                while ( have_posts() ) : the_post(); ?>
                                <?php if( $i > 1 ) : ?> 
                                <div class="w3-row w3-black w3-padding-8"></div>
                                <?php endif; ?>
                                <div class="w3-row <?php echo ( $i % 2 == 0 ) ? 'w3-grey' : 'w3-light-grey'; ?> no-margin">
                                    <div class="w3-container w3-padding-24">
                                    <?php
                                        /*
                                         * Include the Post-Format-specific template for the content.
                                         * If you want to override this in a child theme, then include a file
                                         * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                         */
                                        get_template_part( 'template-parts/content', get_post_format() );
                                    ?>
                                    </div>
                                </div>
                                <?php
                                $i++;
                                endwhile;
//                                the_posts_navigation();
                                ?>
                                    
                        <?php

                        else :

                                get_template_part( 'template-parts/content', 'none' );

                        endif; ?>
                </div>
                <div class="w3-container w3-col m3 s12 padding-top-20">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </main>

<?php
get_footer();
