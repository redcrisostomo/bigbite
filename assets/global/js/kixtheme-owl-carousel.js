jQuery(document).ready(function(){
    /**
     * Frontpage Banner Carousel
     */
    $('.kixtheme-frontpage-banner-carousel').each(function(){
        $(this).owlCarousel({
//        items: 1,
        singleItem: true,
        transitionStyle: "fade",
        //Basic Speeds
        slideSpeed : 200,
        paginationSpeed : 800,
        rewindSpeed : 1000,

        //Autoplay
        autoPlay : $(this).data('autoPlay') > 0 ? true : false,
        autoPlaySpeed: 5000,
        autoPlayTimeout: 5000,
        autoplayHoverPause: false,

        // Navigation
        navigation : true,
        navigationText : ["prev","next"],
        rewindNav : true,
        scrollPerPage : false,

        //Pagination
       pagination : false
//        paginationNumbers: true,
        
        });
    });
    
    /**
     * Event Slider Carousel
     * This is the carousel for article slider
     */
    $('.kixtheme-event-slider-carousel').each(function(){
        $(this).owlCarousel({
        items: $(this).data("column") ? $(this).data("column") : 3,
//        singleItem: true,
//        transitionStyle: "fade",
        //Basic Speeds
        slideSpeed : 200,
        paginationSpeed : 800,
        rewindSpeed : 1000,

        //Autoplay
        autoPlay : true,
        autoPlaySpeed: 5000,
        autoPlayTimeout: 5000,
        autoplayHoverPause: false,

        // Navigation
        navigation : true,
        navigationText : ["prev","next"],
        rewindNav : true,
        scrollPerPage : false,

        //Pagination
        pagination : false
//        paginationNumbers: true,
        
        });
    });
    /**
     * Post Slider Carousel
     * This is the carousel for post slider
     */
    $('.kixtheme-post-slider-carousel').each(function(){
        $(this).owlCarousel({
        items: $(this).data("column") ? $(this).data("column") : 1,
        singleItem: true,
        transitionStyle: "fade",
        //Basic Speeds
        slideSpeed : 200,
        paginationSpeed : 800,
        rewindSpeed : 1000,

        //Autoplay
        autoPlay : true,
        autoPlaySpeed: 5000,
        autoPlayTimeout: 5000,
        autoplayHoverPause: false,

        // Navigation
        navigation : true,
        navigationText : ["prev","next"],
        rewindNav : true,
        scrollPerPage : false,

        //Pagination
        pagination : false
//        paginationNumbers: true,
        
        });
    });
    
    /**
     * Sponsor Slider Carousel
     * This is the carousel for sponsor slider
     */
    $('.kixtheme-sponsor-slider-carousel').each(function(){
        $(this).owlCarousel({
        items: $(this).data("column") ? $(this).data("column") : 1,
//        singleItem: true,
//        transitionStyle: "fade",
        //Basic Speeds
        slideSpeed : 200,
        paginationSpeed : 800,
        rewindSpeed : 1000,

        //Autoplay
        autoPlay : true,
        autoPlaySpeed: 5000,
        autoPlayTimeout: 5000,
        autoplayHoverPause: false,

        // Navigation
        navigation : true,
        navigationText : ["prev","next"],
        rewindNav : true,
        scrollPerPage : false,

        //Pagination
        pagination : false
//        paginationNumbers: true,
        
        });
    });
    /**
     * Article Slider Carousel
     * This is the carousel for article slider
     */
    $('.kixtheme-article-slider-carousel').each(function(){
        $(this).owlCarousel({
        items: $(this).data("column") ? $(this).data("column") : 1,
//        singleItem: true,
//        transitionStyle: "fade",
        //Basic Speeds
        slideSpeed : 200,
        paginationSpeed : 800,
        rewindSpeed : 1000,

        //Autoplay
        autoPlay : true,
        autoPlaySpeed: 5000,
        autoPlayTimeout: 5000,
        autoplayHoverPause: false,

        // Navigation
        navigation : true,
        navigationText : ["prev","next"],
        rewindNav : true,
        scrollPerPage : false

        //Pagination
//        pagination : false
//        paginationNumbers: true,
        
        });
    });
    
});