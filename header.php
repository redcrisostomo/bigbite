<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until main content
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Socialight
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">
<link href="<?php echo get_template_directory_uri();?>/assets/global/css/themes/w3-theme-yellow.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri();?>/assets/global/css/themes/w3-theme-red.css" rel="stylesheet">
<link href="<?php echo get_template_directory_uri();?>/assets/global/css/themes/w3-theme-indigo.css" rel="stylesheet">
<!--<link href="<?php echo get_template_directory_uri();?>assets/global/css/custom.css" rel="stylesheet">-->
<?php if ( is_singular() ) wp_enqueue_script( 'comment_reply' ); ?>
<?php wp_head(); ?>
</head>

<body class="w3-white">
    <header class="w3-row w3-theme-yellow">
        <div class="w3-content w3-container">
            <div class="w3-col m2 s6">
                <div class="w3-center">
                    <a class="site-logo w3-text-theme-white w3-hover-text-black" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo get_template_directory_uri();?>/assets/global/img/logo.jpg" height="66px" /></a>
                            <!--<a class="site-logo w3-text-theme-white w3-hover-text-black" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>-->
                </div>
            </div>
            <nav class="w3-col m8 navbar margin-bottom-0">
                <div class="container">
                    <div class="navbar-header">
                        <a class="navbar-toggle w3-text-white w3-border w3-border-white pull-right" data-target=".navbar-collapse" data-toggle="collapse">
                                <i class="fa fa-bars"></i>
                            </a>
                    </div>
                    <div class="collapse navbar-collapse">
<!--                         <ul class="w3-navbar w3-col m9">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Link 1</a></li>
                            <li class="w3-dropdown-hover">
                              <a href="#">Dropdown <i class="fa fa-caret-down"></i></a>
                              <ul class="w3-dropdown-content w3-white w3-card-4">
                                  <li>
                                      <a href="#">Link 1</a>
                                  </li>
                                  <li>
                                      <a href="#">Link 2</a>
                                  </li>
                                  <li class="w3-dropdown-submenu">
                                      <a href="#">Link 3</a>
                                      <ul class="w3-dropdown-content">
                                          <li><a href="#">Link 1</a></li>
                                          <li><a href="#">Link 2</a></li>
                                          <li class="w3-dropdown-submenu">
                                                <a href="#">Link 3</a>
                                                <ul class="w3-dropdown-content">
                                                    <li><a href="#">Link 1</a></li>
                                                    <li><a href="#">Link 2</a></li>
                                                    <li><a href="#">Link 3</a></li>
                                                </ul>
                                            </li>
                                      </ul>
                                  </li>
                              </ul>
                            </li>
                            <li class="w3-dropdown-hover">
                              <a href="#">Dropdown2 <i class="fa fa-caret-down"></i></a>
                              <ul class="w3-dropdown-content w3-white w3-card-4">
                                  <li>
                                      <a href="#">Link 1</a>
                                  </li>
                                  <li>
                                      <a href="#">Link 2</a>
                                  </li>
                                  <li class="w3-dropdown-submenu">
                                      <a href="#">Link 3</a>
                                      <ul class="w3-dropdown-content">
                                          <li><a href="#">Link 1</a></li>
                                          <li><a href="#">Link 2</a></li>
                                          <li class="w3-dropdown-submenu">
                                                <a href="#">Link 3</a>
                                                <ul class="w3-dropdown-content">
                                                    <li><a href="#">Link 1</a></li>
                                                    <li><a href="#">Link 2</a></li>
                                                    <li><a href="#">Link 3</a></li>
                                                </ul>
                                            </li>
                                      </ul>
                                  </li>
                              </ul>
                            </li>
                          </ul>-->
                        <?php
                            include( 'w3css-walker.php' );
                            wp_nav_menu(
                                array(
                                    'theme_location'    =>  'primary',
                                    'container_class'   =>  'w3-col m6 no-padding',
                                    'menu_class'        =>  'w3-navbar',
                                    'depth'             =>  0,
                                    'walker'            =>  new W3CSS_Walker_Nav_Menu(),
        //                                    'fallback_cb'       => 'humane_wp_page_menu'
                                )
                            );
                        ?>
                        <div class="w3-col m3 no-padding">
                            <ul class="w3-navbar">
                                <li>
                                    <a href="http://localhost:8080/fms/franchisee/" class="w3-theme-indigo-dark w3-padding-16 w3-center">Login as <br/>Franchisee</a>
                                </li>
                                <li>
                                    <a href="http://localhost:8080/fms/franchisor/" class="w3-theme-red-d1 w3-padding-16 w3-center">Login as <br/> Franchisor</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
                <!-- END NAVIGATION -->
                
                <!-- END NAVIGATION -->
            
<!--                        <div class="w3-col m3 no-padding">
                            <ul class="nav navbar-nav">
                                <li class="dropdown">
                                    <a href="#" data-toggle="dropdown" class="w3-padding-24 w3-text-theme w3-hover-theme-d5"><i class="fa fa-search"></i></a>
                                    <ul class="dropdown-menu">
                                        <div class="w3-text-theme-d5"><?php get_search_form(); ?></div>
                                    </ul>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav">
                                <?php if( !is_user_logged_in() ) : ?> 
                                <li><a href="<?php echo site_url(); ?>/wp-login.php" class="w3-text-theme w3-hover-text-theme-d3 w3-padding-24">Log In</a></li>
                                <li><a href="<?php echo site_url(); ?>/wp-register.php" class="w3-text-theme w3-hover-text-theme-d3 w3-padding-24">Registration</a></li>
                                <?php endif; ?>
                            </ul>
                        
                        </div>-->
        <!-- Header END -->
        </div>
    </header>
    <section class="margin-bottom-0">
        <?php
            get_template_part( 'template-parts/custom', 'frontpage-banner' );
//            Banner::init();
        ?>
    </section>