<?php

/**
 * BEGIN CONTACT INFO SETTINGS
 */

add_action( 'admin_menu','add_settings_contact_info' );
function add_settings_contact_info(){
    add_submenu_page('options-general.php', 'Contact Info', 'Contact Info', 7, 'contact_info','contact_settings_init');
}

function contact_settings_init() { 
    ?>
    <form method = "post" action = "options.php" >
        <?php 
            settings_fields( 'contact_info_group' );
            do_settings_sections( 'contact_info_group' );
            submit_button(); 
        ?>
    </form>
<?php
}


/**
 * Create form fields for contact info
 */
function form_contact_info( array $args ){
?>
    <!-- Form: Theme Color -->
    <?php if ( 'address' == $args['form'] ) : ?>
        <textarea name="address" id="address" class="regular-text" ><?php echo get_option( 'address' ); ?></textarea>    
    <!-- Form: Contact Number -->
    <?php elseif('contact_number_1' == $args['form']) :?>
        <input type="text" name="contact_number_1" id="contact_number_1" value="<?php echo get_option( 'contact_number_1' ); ?>" />
    <?php elseif('contact_number_2' == $args['form']) :?>
        <input type="text" name="contact_number_2" id="contact_number_2" value="<?php echo get_option( 'contact_number_2' ); ?>" />
    <?php elseif('contact_number_3' == $args['form']) :?>
        <input type="text" name="contact_number_3" id="contact_number_3" value="<?php echo get_option( 'contact_number_3' ); ?>" />
    <?php elseif('email_1' == $args['form']) :?>
        <input type="email" name="email_1" id="email_1" value="<?php echo get_option( 'email_1' ); ?>" />
    <?php elseif('email_2' == $args['form']) :?>
        <input type="email" name="email_2" id="email_2" value="<?php echo get_option( 'email_2' ); ?>" />
<?php endif; /* end if($args['form']== ???) */

} /*End of function form_contact_info*/


/**
 * Add section and register the created form fields to the section
 */
function settings_contact_info() {
    add_settings_section(
        "contact_info_section",      // id
        "Contact Info",      // title
        null,                   // callback
        "contact_info_group" // page
    );
    
    add_settings_field(
        "address",                      // id
        "Address",                      // title
        "form_contact_info",             // callback
        "contact_info_group",            // page
        "contact_info_section",                  // section
        array( 'form' => 'address' )    // args
    );
    add_settings_field(
        "contact_number_1",                      // id
        "Contact Number",                      // title
        "form_contact_info",             // callback
        "contact_info_group",            // page
        "contact_info_section",                  // section
        array( 'form' => 'contact_number_1' )    // args
    );
    add_settings_field(
        "contact_number_2",                      // id
        "Additional Contact",                      // title
        "form_contact_info",             // callback
        "contact_info_group",            // page
        "contact_info_section",                  // section
        array( 'form' => 'contact_number_2' )    // args
    );
    add_settings_field(
        "contact_number_3",                      // id
        "Additional Contact",                      // title
        "form_contact_info",             // callback
        "contact_info_group",            // page
        "contact_info_section",                  // section
        array( 'form' => 'contact_number_3' )    // args
    );
    add_settings_field(
        "email_1",                      // id
        "Email",                      // title
        "form_contact_info",             // callback
        "contact_info_group",            // page
        "contact_info_section",                  // section
        array( 'form' => 'email_1' )    // args
    );
    add_settings_field(
        "email_2",                      // id
        "Additional Email",                      // title
        "form_contact_info",             // callback
        "contact_info_group",            // page
        "contact_info_section",                  // section
        array( 'form' => 'email_2' )    // args
    );
    
    register_setting( "contact_info_group", "address" );
    register_setting( "contact_info_group", "contact_number_1" );
    register_setting( "contact_info_group", "contact_number_2" );
    register_setting( "contact_info_group", "contact_number_3" );
    register_setting( "contact_info_group", "email_1" );
    register_setting( "contact_info_group", "email_2" );
}

/* Hook the function to an action */
add_action( "admin_init", "settings_contact_info" );

/**
 * END CONTACT INFO SETINGS
 */


/**
 * BEGIN SOCIAL MEDIA SETTINGS
 */

add_action( 'admin_menu','add_settings_social_media' );
function add_settings_social_media(){
    add_submenu_page('options-general.php', 'Social Media', 'Social Media', 7, 'social_media','social_media_settings_init');
}

function social_media_settings_init() { 
    ?>
    <form method = "post" action = "options.php" >
        <?php 
            settings_fields( 'social_media_settings_group' );
            do_settings_sections( 'social_media_settings_group' );
            submit_button(); 
        ?>
    </form>
<?php
}

/**
 * Create form fields for Social Media
 */
/*Get the list of social media */
    $json = file_get_contents( get_template_directory()."/social-media-accounts.json" );
    $social_media = json_decode( $json, true );
function form_social_media_settings( array $args ){
   global $social_media;
?>
    <!-- Form: Social Media  -->
    <?php foreach ( $social_media['accounts'] as $account ) : ?>
        <?php if ( $account['id'] == $args['form'] ) : ?>
    <input type="url" name="<?php echo $account['id']; ?>" id="<?php echo $account['id']; ?>" value="<?php echo get_option( $account['id'] ); ?>" class="regular-text"/>
        <?php endif; /* end if( ??? == $args['form'] ) */ ?>
    <?php endforeach; //$social_media['account'] as $account 
} /*End of function form_social_media_settings*/


/**
 * Add section and register the created form fields to the section
 */
function social_media_settings() {
    global $social_media;
    
    add_settings_section(
        "social_media_section",         // id
        "Social Media Accounts",        // title
        null,                           // callback
        "social_media_settings_group"    // page
    );
    
    foreach( $social_media['accounts'] as $account ) {
        add_settings_field(
            $account['id'],                         // id
            $account['title'],                      // title
            "form_social_media_settings",            // callback
            "social_media_settings_group",           // page
            "social_media_section",                 // section
            array( 'form' => $account['id'] )       // args
        );
        register_setting( "social_media_settings_group", $account['id'] );
    }
}

/* Hook the function to an action */
add_action( "admin_init", "social_media_settings" );

/**
 * END SOCIAL MEDIA OPTIONS
 */