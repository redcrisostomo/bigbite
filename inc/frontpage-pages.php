<?php
/**
 * Frontpage Banner by KixTheme
 * 
 * @link https://codex.wordpress.org/Creating_Options_Pages
 *
 * @package Bigbite
 * @subpackage Frontpage Banner
 * @author Frederick S. Crisostomo
 */


/**
* Registers the Frontpage Banner custom post type
*/
function frontpage_pages_register() {
    $args = array(
            'public'                => true,
            'label'                 => 'Frontpage Pages',
            'public'                => false,
            'exclude_from_search'   => true,
            'publicly_queryable'    => false,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'show_in_admin_bar'     => false,
            'menu_position'         => 5,
            'menu_icon'             => 'dashicons-admin-page',
            'query_var'             => true,
            'rewrite'               => false,
            'capability_type'       => 'post',
            'has_archive'           => false,
            'hierarchical'          => false,
            'can_export'            => true,
            'query_var'             => false,
            'capability_type'       => 'post',
            'supports'              => array( 'title', 'page-attributes' ),
            'taxonomies'            => array( 'frontpage_pages_categories'),
            'register_meta_box_cb'  => 'add_frontpage_pages_id_metabox'
    );
    register_post_type( 'frontpage_pages', $args );
}
add_post_type_support( 'frontpage_pages', 'thumbnail' );    
add_action( 'init', 'frontpage_pages_register' );

/**
* Register meta box(es).
*/
function add_frontpage_pages_title2_metabox() {
       add_meta_box( 'meta-box-date', __( 'Title2', 'wp_frontpage_pages' ), 'frontpage_pages_metabox_title2', 'frontpage_pages', 'normal', 'high' );
}
add_action( 'add_meta_boxes', 'add_frontpage_pages_title2_metabox' );
function add_frontpage_pages_id_metabox() {
       add_meta_box( 'meta-box-id', __( 'Page ID', 'wp_frontpage_pages' ), 'frontpage_pages_metabox_id', 'frontpage_pages', 'side', 'high' );
}
add_action( 'add_meta_boxes', 'add_frontpage_pages_id_metabox' );

function add_frontpage_pages_background_color_metabox() {
       add_meta_box( 'meta-box-background-color', __( 'Background Color', 'wp_frontpage_pages' ), 'frontpage_pages_background_color', 'frontpage_pages', 'side', 'high' );
}
add_action( 'add_meta_boxes', 'add_frontpage_pages_background_color_metabox' );

function add_frontpage_pages_title_color_metabox() {
       add_meta_box( 'meta-box-title-color', __( 'Title Text Color', 'wp_frontpage_pages' ), 'frontpage_pages_title_color', 'frontpage_pages', 'side', 'high' );
}
add_action( 'add_meta_boxes', 'add_frontpage_pages_title_color_metabox' );

function add_frontpage_pages_desc() {
       add_meta_box( 'meta-box-desc', __( 'Item Description', 'wp_frontpage_pages' ), 'frontpage_pages_metabox_desc', 'frontpage_pages', 'normal' );
}
add_action( 'add_meta_boxes', 'add_frontpage_pages_desc' );

function add_frontpage_pages_image_per_row() {
       add_meta_box( 'meta-box-image-per-row', __( 'No of Image per row', 'wp_frontpage_pages' ), 'frontpage_pages_metabox_image_per_row', 'frontpage_pages', 'side' );
}
add_action( 'add_meta_boxes', 'add_frontpage_pages_image_per_row' );

/**
* Meta box display callback.
*
* @param WP_Post $post Current post object.
*/

function frontpage_pages_metabox_title2( $post ) {
       // Display code/markup goes here. Don't forget to include nonces!
       // Noncename needed to verify where the data originated
       echo '<input type="hidden" name="wp_frontpage_pages_title2_nonce" id="wp_frontpage_pages_title2_nonce" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
       // Get the url data if its already been entered
       $meta_value = get_post_meta( get_the_ID(), 'wp_frontpage_pages_title2', true );
       // Checks and displays the retrieved value
//       echo '<input type="text" name="wp_events_date" value="' . $meta_value  . '" class="widefat datepicker" />';
       echo '<input type="text" name="wp_frontpage_pages_title2" value="' . $meta_value  . '" class="widefat" />';
}
function frontpage_pages_metabox_id( $post ) {
       // Display code/markup goes here. Don't forget to include nonces!
       // Noncename needed to verify where the data originated
       echo '<input type="hidden" name="wp_frontpage_pages_nonce" id="wp_frontpage_pages_nonce" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
       // Get the url data if its already been entered
       $meta_value = get_post_meta( get_the_ID(), 'wp_frontpage_pages_id', true );
       // Checks and displays the retrieved value
       echo '<input type="text" name="wp_frontpage_pages_id" value="' . $meta_value  . '" class="widefat" />';
}
function frontpage_pages_background_color( $post) {
       // Display code/markup goes here. Don't forget to include nonces!
       // Noncename needed to verify where the data originated
       echo '<input type="hidden" name="wp_frontpage_pages_background_color_nonce" id="wp_frontpage_pages_background_color_nonce" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
       // Get the url data if its already been entered
       $meta_value = get_post_meta( get_the_ID(), 'wp_frontpage_pages_background_color', true );
       // Checks and displays the retrieved value
       echo '<select type="checkbox" name="wp_frontpage_pages_background_color" class="widefat">';
       echo sprintf('<option value="theme-yellow-d1" %s >Yellow</option>', 'theme-yellow-d1' == $meta_value ? 'Selected="selected"' : null);
       echo sprintf('<option value="theme-red-d1" %s>Red</option>', 'theme-red-d1' == $meta_value ? 'Selected="selected"' : null);
       echo sprintf('<option value="theme-indigo-d1" %s>Indigo</option>', 'theme-indigo-d1' == $meta_value ? 'Selected="selected"' : null);
       echo sprintf('<option value="white" %s>White</option>', 'white' == $meta_value ? 'Selected="selected"' : null);
       echo sprintf('<option value="grey" %s>Grey</option>', 'grey' == $meta_value ? 'Selected="selected"' : null);
       echo sprintf('<option value="dark-grey" %s>Dark Grey</option>', 'dark-grey' == $meta_value ? 'Selected="selected"' : null);
       echo sprintf('<option value="black" %s>Black</option>', 'black' == $meta_value ? 'Selected="selected"' : null);
       echo '</select>';
}

function frontpage_pages_title_color( $post ) {
       // Display code/markup goes here. Don't forget to include nonces!
       // Noncename needed to verify where the data originated
       echo '<input type="hidden" name="wp_frontpage_pages_title_color_nonce" id="wp_frontpage_pages_title_color_nonce" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
       // Get the url data if its already been entered
       $meta_value = get_post_meta( get_the_ID(), 'wp_frontpage_pages_title_color', true );
       // Checks and displays the retrieved value
       echo '<select type="checkbox" name="wp_frontpage_pages_title_color" class="widefat">';
       echo sprintf('<option value="theme-yellow-d1" %s >Yellow</option>', 'theme-yellow-d1' == $meta_value ? 'Selected="selected"' : null);
       echo sprintf('<option value="theme-red-d1" %s>Red</option>', 'theme-red-d1' == $meta_value ? 'Selected="selected"' : null);
       echo sprintf('<option value="theme-indigo-d1" %s>Indigo</option>', 'theme-indigo-d1' == $meta_value ? 'Selected="selected"' : null);
       echo sprintf('<option value="white" %s>White</option>', 'white' == $meta_value ? 'Selected="selected"' : null);
       echo sprintf('<option value="grey" %s>Grey</option>', 'grey' == $meta_value ? 'Selected="selected"' : null);
       echo sprintf('<option value="dark-grey" %s>Dark Grey</option>', 'dark-grey' == $meta_value ? 'Selected="selected"' : null);
       echo sprintf('<option value="black" %s>Black</option>', 'black' == $meta_value ? 'Selected="selected"' : null);
       echo '</select>';
}

function frontpage_pages_metabox_desc( $post ) {
       // Display code/markup goes here. Don't forget to include nonces!
       // Noncename needed to verify where the data originated
       echo '<input type="hidden" name="wp_frontpage_pages_desc_nonce" id="wp_frontpage_pages_desc_nonce" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
       // Get the url data if its already been entered
       $meta_value = get_post_meta( get_the_ID(), 'wp_frontpage_pages_desc', true );
       $meta_value = apply_filters('the_content', $meta_value);
       $meta_value = str_replace(']]>', ']]>', $meta_value);
       // Checks and displays the retrieved value
       $editor_settings = array( 'wpautop' => true, 'media_buttons' => false, 'textarea_rows' => '8', 'textarea_name' => 'wp_frontpage_pages_desc');
       echo wp_editor($meta_value, 'wp_frontpage_pages_desc', $editor_settings);
}
function frontpage_pages_metabox_image_per_row( $post ) {
       // Display code/markup goes here. Don't forget to include nonces!
       // Noncename needed to verify where the data originated
       echo '<input type="hidden" name="wp_frontpage_pages_image_per_row_nonce" id="wp_frontpage_pages_image_per_row_nonce" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
       // Get the url data if its already been entered
       $meta_value = get_post_meta( get_the_ID(), 'wp_frontpage_pages_image_per_row', true );
       // Checks and displays the retrieved value
       echo '<input type="text" name="wp_frontpage_pages_image_per_row" value="' . $meta_value  . '" class="widefat" />';
}



/**
* Save meta box content.
*
* @param int $post_id Post ID
*/
function frontpage_pages_save_metabox( $post_id ) {
       // verify this came from the our screen and with proper authorization,
       // because save_post can be triggered at other times

       // Checks save status
       $is_autosave = wp_is_post_autosave( $post_id );
       $is_revision = wp_is_post_revision( $post_id );
       $is_valid_nonce = ( isset( $_POST[ 'wp_frontpage_pages_nonce' ] ) && wp_verify_nonce( $_POST[ 'wp_frontpage_pages_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
       // Exits script depending on save status
       if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
               return;
       }
       // Checks for input and sanitizes/saves if needed
       if( isset( $_POST[ 'wp_frontpage_pages_title2' ] ) ) {
               update_post_meta( $post_id, 'wp_frontpage_pages_title2', sanitize_text_field( $_POST[ 'wp_frontpage_pages_title2' ] ) );
       }
       if( isset( $_POST[ 'wp_frontpage_pages_id' ] ) ) {
               update_post_meta( $post_id, 'wp_frontpage_pages_id', sanitize_text_field( $_POST[ 'wp_frontpage_pages_id' ] ) );
       }
       if( isset( $_POST[ 'wp_frontpage_pages_background_color' ] ) ) {
               update_post_meta( $post_id, 'wp_frontpage_pages_background_color', sanitize_text_field( $_POST[ 'wp_frontpage_pages_background_color' ] ) );
       }
       if( isset( $_POST[ 'wp_frontpage_pages_title_color' ] ) ) {
               update_post_meta( $post_id, 'wp_frontpage_pages_title_color', sanitize_text_field( $_POST[ 'wp_frontpage_pages_title_color' ] ) );
       }
       if( isset( $_POST[ 'wp_frontpage_pages_desc' ] ) ) {
               update_post_meta( $post_id, 'wp_frontpage_pages_desc', $_POST[ 'wp_frontpage_pages_desc' ] );
       }
       if( isset( $_POST[ 'wp_frontpage_pages_image_per_row' ] ) ) {
               update_post_meta( $post_id, 'wp_frontpage_pages_image_per_row', $_POST[ 'wp_frontpage_pages_image_per_row' ] );
       }
}
add_action( 'save_post', 'frontpage_pages_save_metabox' );

/**
* Adds a new column to the Frontpage Banner overview list in the dashboard
*/
function frontpage_pages_add_new_column($defaults) {
       $defaults['wp_frontpage_pages_title2'] = __('Title 2', 'wp_frontpage_pages');
       $defaults['wp_frontpage_pages_image'] = __('Item image', 'wp_frontpage_pages');
       $defaults['menu_order'] = __('Order', 'wp_frontpage_pages');
       return $defaults;
}
add_filter('manage_frontpage_pages_posts_columns', 'frontpage_pages_add_new_column');

/**
* Adds the frontpage_pages title 2 (if available) to the Frontpage Banner overview list in the dashboard
*/
function frontpage_pages_column_add_title2($column_name, $post_ID) {
       if ($column_name == 'wp_frontpage_pages_title2') {
               echo get_post_meta( $post_ID, 'wp_frontpage_pages_title2', true );
//               echo date( 'M d, Y', strtotime( get_post_meta( $post_ID, 'wp_events_date', true ) ) );
       }
}
add_action('manage_frontpage_pages_posts_custom_column', 'frontpage_pages_column_add_title2', 10, 2 );

/**
* Adds the frontpage_pages image (if available) to the Frontpage Banner overview list in the dashboard
*/
function frontpage_pages_column_add_image($column_name, $post_ID) {
       if ($column_name == 'wp_frontpage_pages_image') {
               $image = get_the_post_thumbnail( $post_ID );
               echo $image;
       }
}
add_action('manage_frontpage_pages_posts_custom_column', 'frontpage_pages_column_add_image', 10, 2);

/**
* show custom order column values
*/
function frontpage_pages_column_add_order($name){
       global $post;

       switch ($name) {
               case 'menu_order':
                       $order = $post->menu_order;
                       echo $order;
                       break;
               default:
                       break;
       }
}
add_action('manage_frontpage_pages_posts_custom_column','frontpage_pages_column_add_order');


function frontpage_pages_order_column($columns){
       $columns['menu_order'] = 'menu_order';
       return $columns;
}
add_filter('manage_edit-frontpage_pages_sortable_columns','frontpage_pages_order_column');

function frontpage_pages_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Title' ),
		'wp_frontpage_pages_title2' => __( 'Title 2' ),
		'date' => __( 'Date' ),
		'menu_order' => __( 'Order' ),
	);

	return $columns;
}
add_filter( 'manage_edit-frontpage_pages_columns', 'frontpage_pages_columns' ) ;

function frontpage_pages_set_featured_image_filter(){
   $screen = get_current_screen();
   if( isset($screen->post_type) && $screen->post_type == 'frontpage_pages'){
       add_filter( 'admin_post_thumbnail_html', 'frontpage_pages_change_featured_image_strings', 10, 1);
   }
}

function frontpage_pages_change_featured_image_strings($content) {
   $content = str_replace(__('Featured Image'), __('Set item image', 'wp-frontpage_pages'), $content);
   $content = str_replace(__('Set featured image'), __('Set item image', 'wp-frontpage_pages'), $content);
   $content = str_replace(__('Remove featured image'), __('Remove item image', 'wp-frontpage_pages'), $content);

   return $content;
}

add_action('current_screen', 'frontpage_pages_set_featured_image_filter');

function frontpage_pages_change_meta_box_title() {
   remove_meta_box( 'postimagediv', 'frontpage_pages', 'side' ); //replace post_type from your post type name
   add_meta_box('postimagediv', __('Item image', 'wp-frontpage_pages'), 'post_thumbnail_meta_box', 'frontpage_pages', 'side', 'high');
}

add_action( 'admin_head', 'frontpage_pages_change_meta_box_title' );

function create_frontpage_pages_post()
  {
   //post status and options
    $post = array(
          'comment_status' => 'closed',
          'ping_status' =>  'closed' ,
          'post_date' => date('Y-m-d H:i:s'),
          'post_name' => 'frontpage_pages',
          'post_status' => 'publish' ,
          'post_title' => 'Frontpage Banner',
          'post_type' => 'post',
    );
    //insert page and save the id
    $newvalue = wp_insert_post( $post, false );
    //save the id in the database
    update_option( 'frontpage_pages', $newvalue );
  }

  register_activation_hook( __FILE__, 'create_frontpage_pages_post');
  
  if (class_exists('MultiPostThumbnails')) {
      for($i = 1; $i <= 8; $i++) {
        new MultiPostThumbnails(
            array(
                'label' => 'Image '.$i,
                'id' => 'image-'.$i,
                'post_type' => 'frontpage_pages'
            )
        );
      }
}