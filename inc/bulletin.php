<?php
/**
 * Bulletin by KixTheme
 * 
 * @link https://codex.wordpress.org/Creating_Options_Pages
 *
 * @package Bigbite
 * @subpackage Bulletin
 * @author Frederick S. Crisostomo
 */

/**
 * Add bulletin menu
 */

function register_bulletin() {
 
    $labels = array(
        'name' => _x( 'Bulletin', 'bulletin' ),
        'singular_name' => _x( 'Bulletin', 'bulletin' ),
        'all_items' => _x( 'All Bulletin', 'bulletin' ),
        'add_new' => _x( 'Add New', 'bulletin' ),
        'add_new_item' => _x( 'Add Bulletin', 'bulletin' ),
        'edit_item' => _x( 'Edit Bulletin', 'bulletin' ),
        'new_item' => _x( 'New Item', 'bulletin' ),
        'view_item' => _x( 'View Bulletin', 'bulletin' ),
        'search_items' => _x( 'Search Bulletin', 'bulletin' ),
        'not_found' => _x( 'No bulletin found', 'bulletin' ),
        'not_found_in_trash' => _x( 'No bulletin found in Trash', 'bulletin' ),
        'parent_item_colon' => _x( 'Parent Bulletin:', 'bulletin' ),
        'menu_name' => _x( 'Bulletin', 'bulletin' ),
    );
 
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'description' => 'Bulletin items',
        'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'page-attributes' ),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-welcome-write-blog',
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );
 
    register_post_type( 'bulletin', $args );
    flush_rewrite_rules( false );
}
 
add_action( 'init', 'register_bulletin' );


function create_bulletin_post()
  {
   //post status and options
    $post = array(
          'comment_status' => 'open',
          'ping_status' =>  'closed' ,
          'post_date' => date('Y-m-d H:i:s'),
          'post_name' => 'bulletin',
          'post_status' => 'publish' ,
          'post_title' => 'Bulletin',
          'post_type' => 'post',
    );
    //insert page and save the id
    $newvalue = wp_insert_post( $post, false );
    //save the id in the database
    update_option( 'bulletin', $newvalue );
  }

  register_activation_hook( __FILE__, 'create_bulletin_post');