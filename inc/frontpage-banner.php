<?php
/**
 * Frontpage Banner by KixTheme
 * 
 * @link https://codex.wordpress.org/Creating_Options_Pages
 *
 * @package Hexad
 * @subpackage Frontpage Banner
 * @author Frederick S. Crisostomo
 */


/**
* Registers the Frontpage Banner custom post type
*/
function frontpage_banner_register() {
    $args = array(
            'public'                => true,
            'label'                 => 'Frontpage Banner',
            'public'                => false,
            'exclude_from_search'   => true,
            'publicly_queryable'    => false,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'show_in_admin_bar'     => false,
            'menu_position'         => 5,
            'menu_icon'             => 'dashicons-category',
            'query_var'             => true,
            'rewrite'               => false,
            'capability_type'       => 'post',
            'has_archive'           => false,
            'hierarchical'          => false,
            'can_export'            => true,
            'query_var'             => false,
            'capability_type'       => 'post',
            'supports'              => array( 'title', 'page-attributes' ),
            'taxonomies'            => array( 'frontpage_banner_categories'),
            'register_meta_box_cb'  => 'add_frontpage_banner_metabox'
    );
    register_post_type( 'frontpage_banner', $args );
}
add_post_type_support( 'frontpage_banner', 'thumbnail' );    
add_action( 'init', 'frontpage_banner_register' );

/**
* Register meta box(es).
*/
function add_frontpage_banner_title2_metabox() {
       add_meta_box( 'meta-box-date', __( 'Title2', 'wp_frontpage_banner' ), 'frontpage_banner_metabox_title2', 'frontpage_banner', 'normal', 'high' );
}
add_action( 'add_meta_boxes', 'add_frontpage_banner_title2_metabox' );
function add_frontpage_banner_metabox() {
       add_meta_box( 'meta-box-id', __( 'Item URL', 'wp_frontpage_banner' ), 'frontpage_banner_metabox_url', 'frontpage_banner', 'side', 'high' );
}
add_action( 'add_meta_boxes', 'add_frontpage_banner_metabox' );

function add_frontpage_banner_hide_title_metabox() {
       add_meta_box( 'meta-box-hide-title', __( 'Hide Title', 'wp_frontpage_banner' ), 'frontpage_banner_hide_title', 'frontpage_banner', 'side', 'high' );
}
add_action( 'add_meta_boxes', 'add_frontpage_banner_hide_title_metabox' );

function add_frontpage_banner_desc() {
       add_meta_box( 'meta-box-desc', __( 'Item Description', 'wp_frontpage_banner' ), 'frontpage_banner_metabox_desc', 'frontpage_banner', 'normal' );
}
add_action( 'add_meta_boxes', 'add_frontpage_banner_desc' );

/**
* Meta box display callback.
*
* @param WP_Post $post Current post object.
*/

function frontpage_banner_metabox_title2( $post ) {
       // Display code/markup goes here. Don't forget to include nonces!
       // Noncename needed to verify where the data originated
       echo '<input type="hidden" name="wp_frontpage_banner_title2_nonce" id="wp_frontpage_banner_title2_nonce" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
       // Get the url data if its already been entered
       $meta_value = get_post_meta( get_the_ID(), 'wp_frontpage_banner_title2', true );
       // Checks and displays the retrieved value
//       echo '<input type="text" name="wp_events_date" value="' . $meta_value  . '" class="widefat datepicker" />';
       echo '<input type="text" name="wp_frontpage_banner_title2" value="' . $meta_value  . '" class="widefat" />';
}
function frontpage_banner_metabox_url( $post ) {
       // Display code/markup goes here. Don't forget to include nonces!
       // Noncename needed to verify where the data originated
       echo '<input type="hidden" name="wp_frontpage_banner_nonce" id="wp_frontpage_banner_nonce" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
       // Get the url data if its already been entered
       $meta_value = get_post_meta( get_the_ID(), 'wp_frontpage_banner_url', true );
       // Checks and displays the retrieved value
       echo '<input type="text" name="wp_frontpage_banner_url" value="' . $meta_value  . '" class="widefat" />';
}
function frontpage_banner_hide_title( $post ) {
       // Display code/markup goes here. Don't forget to include nonces!
       // Noncename needed to verify where the data originated
       echo '<input type="hidden" name="wp_frontpage_banner_hide_title_nonce" id="wp_frontpage_banner_hide_title_nonce" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
       // Get the url data if its already been entered
       $meta_value = get_post_meta( get_the_ID(), 'wp_frontpage_banner_hide_title', true );
       // Checks and displays the retrieved value
       echo sprintf('<input type="checkbox" name="wp_frontpage_banner_hide_title" value="1" class="widefat" %s />', isset( $_POST[ 'wp_frontpage_banner_hide_title' ] ) ? "Checked='checked'" : null);
}

function frontpage_banner_metabox_desc( $post ) {
       // Display code/markup goes here. Don't forget to include nonces!
       // Noncename needed to verify where the data originated
       echo '<input type="hidden" name="wp_frontpage_banner_desc_nonce" id="wp_frontpage_banner_desc_nonce" value="' . wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
       // Get the url data if its already been entered
       $meta_value = get_post_meta( get_the_ID(), 'wp_frontpage_banner_desc', true );
       $meta_value = apply_filters('the_content', $meta_value);
       $meta_value = str_replace(']]>', ']]>', $meta_value);
       // Checks and displays the retrieved value
       $editor_settings = array( 'wpautop' => true, 'media_buttons' => false, 'textarea_rows' => '8', 'textarea_name' => 'wp_frontpage_banner_desc');
       echo wp_editor($meta_value, 'wp_frontpage_banner_desc', $editor_settings);
}


/**
* Save meta box content.
*
* @param int $post_id Post ID
*/
function frontpage_banner_save_metabox( $post_id ) {
       // verify this came from the our screen and with proper authorization,
       // because save_post can be triggered at other times

       // Checks save status
       $is_autosave = wp_is_post_autosave( $post_id );
       $is_revision = wp_is_post_revision( $post_id );
       $is_valid_nonce = ( isset( $_POST[ 'wp_frontpage_banner_nonce' ] ) && wp_verify_nonce( $_POST[ 'wp_frontpage_banner_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
       // Exits script depending on save status
       if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
               return;
       }
       // Checks for input and sanitizes/saves if needed
       if( isset( $_POST[ 'wp_frontpage_banner_title2' ] ) ) {
               update_post_meta( $post_id, 'wp_frontpage_banner_title2', sanitize_text_field( $_POST[ 'wp_frontpage_banner_title2' ] ) );
       }
       if( isset( $_POST[ 'wp_frontpage_banner_url' ] ) ) {
               update_post_meta( $post_id, 'wp_frontpage_banner_url', sanitize_text_field( $_POST[ 'wp_frontpage_banner_url' ] ) );
       }
       if( isset( $_POST[ 'wp_frontpage_banner_hide_title' ] ) ) {
               update_post_meta( $post_id, 'wp_frontpage_banner_hide_title', sanitize_text_field( $_POST[ 'wp_frontpage_banner_hide_title' ] ) );
       }
       if( isset( $_POST[ 'wp_frontpage_banner_desc' ] ) ) {
               update_post_meta( $post_id, 'wp_frontpage_banner_desc', $_POST[ 'wp_frontpage_banner_desc' ] );
       }
}
add_action( 'save_post', 'frontpage_banner_save_metabox' );

/**
* Adds a new column to the Frontpage Banner overview list in the dashboard
*/
function frontpage_banner_add_new_column($defaults) {
       $defaults['wp_frontpage_banner_title2'] = __('Title 2', 'wp_frontpage_banner');
       $defaults['wp_frontpage_banner_image'] = __('Item image', 'wp_frontpage_banner');
       $defaults['menu_order'] = __('Order', 'wp-frontpage_banner');
       return $defaults;
}
add_filter('manage_frontpage_banner_posts_columns', 'frontpage_banner_add_new_column');

/**
* Adds the frontpage_banner title 2 (if available) to the Frontpage Banner overview list in the dashboard
*/
function frontpage_banner_column_add_title2($column_name, $post_ID) {
       if ($column_name == 'wp_frontpage_banner_title2') {
               echo get_post_meta( $post_ID, 'wp_frontpage_banner_title2', true );
//               echo date( 'M d, Y', strtotime( get_post_meta( $post_ID, 'wp_events_date', true ) ) );
       }
}
add_action('manage_frontpage_banner_posts_custom_column', 'frontpage_banner_column_add_title2', 10, 2 );

/**
* Adds the frontpage_banner image (if available) to the Frontpage Banner overview list in the dashboard
*/
function frontpage_banner_column_add_image($column_name, $post_ID) {
       if ($column_name == 'wp_frontpage_banner_image') {
               $image = get_the_post_thumbnail( $post_ID );
               echo $image;
       }
}
add_action('manage_frontpage_banner_posts_custom_column', 'frontpage_banner_column_add_image', 10, 2);

/**
* show custom order column values
*/
function frontpage_banner_column_add_order($name){
       global $post;

       switch ($name) {
               case 'menu_order':
                       $order = $post->menu_order;
                       echo $order;
                       break;
               default:
                       break;
       }
}
add_action('manage_frontpage_banner_posts_custom_column','frontpage_banner_column_add_order');


function frontpage_banner_order_column($columns){
       $columns['menu_order'] = 'menu_order';
       return $columns;
}
add_filter('manage_edit-frontpage_banner_sortable_columns','frontpage_banner_order_column');

function frontpage_banner_set_featured_image_filter(){
   $screen = get_current_screen();
   if( isset($screen->post_type) && $screen->post_type == 'frontpage_banner'){
       add_filter( 'admin_post_thumbnail_html', 'frontpage_banner_change_featured_image_strings', 10, 1);
   }
}

function frontpage_banner_change_featured_image_strings($content) {
   $content = str_replace(__('Featured Image'), __('Set item image', 'wp-frontpage_banner'), $content);
   $content = str_replace(__('Set featured image'), __('Set item image', 'wp-frontpage_banner'), $content);
   $content = str_replace(__('Remove featured image'), __('Remove item image', 'wp-frontpage_banner'), $content);

   return $content;
}

add_action('current_screen', 'frontpage_banner_set_featured_image_filter');

function frontpage_banner_change_meta_box_title() {
   remove_meta_box( 'postimagediv', 'frontpage_banner', 'side' ); //replace post_type from your post type name
   add_meta_box('postimagediv', __('Item image', 'wp-frontpage_banner'), 'post_thumbnail_meta_box', 'frontpage_banner', 'side', 'high');
}

add_action( 'admin_head', 'frontpage_banner_change_meta_box_title' );

function create_frontpage_banner_post()
  {
   //post status and options
    $post = array(
          'comment_status' => 'closed',
          'ping_status' =>  'closed' ,
          'post_date' => date('Y-m-d H:i:s'),
          'post_name' => 'frontpage_banner',
          'post_status' => 'publish' ,
          'post_title' => 'Frontpage Banner',
          'post_type' => 'post',
    );
    //insert page and save the id
    $newvalue = wp_insert_post( $post, false );
    //save the id in the database
    update_option( 'frontpage_banner', $newvalue );
  }

  register_activation_hook( __FILE__, 'create_frontpage_banner_post');
  
  /*shortcodes*/
  function fms_button($atts) {
    extract(shortcode_atts(array(
        'color' => '',
        'text' => '',
        'url' => '',
        'uppercase' => false,
   ), $atts));
    
    $output = sprintf("<a class='w3-btn %s %s' href='%s'> %s </a>", $color, true==$uppercase OR 'true' == strtolower($uppercase) ? 'uppercase' : null, $url, $text); 
    echo do_shortcode($output);
}
add_shortcode( 'fms_button', 'fms_button' );