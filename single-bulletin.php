<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package kixtheme
 */

get_header(); ?>
<div class="w3-row">
    <?php get_template_part( 'template-parts/custom', 'frontpage-banner' );?>
</div>
<div class="w3-row w3-yellow w3-padding-16">
    <header>
        <h3 class="w3-xxlarge bold w3-center w3-text-white">Bulletin</h3>
    </header>
</div>
<div class="w3-row">
	<div id="primary" class="w3-content">
            <div class="w3-row w3-padding-24">
		<main id="main" class="site-main news padding-right-15" role="main">
		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_format() );
                ?>
                <?php

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>
		</main><!-- #main -->
                <div class="w3-col m4">
                    <div class="search-login">
                        <!-- BEGIN SEARCH BOX -->
                        <?php get_template_part( 'template-parts/search-box' ); ?>
                        <!-- END SEARCH BOX -->
                        <!-- BEGIN LOGIN SECTION -->
                        <div>
                            <?php get_template_part( 'template-parts/custom-login' ); ?>
                            <?php $bgcolor="w3-blue"; include( locate_template( 'template-parts/button-club.php' ) ); ?>
                            <?php $bgcolor="w3-yellow"; include( locate_template( 'template-parts/button-instructor.php' ) ); ?>
                        </div>
                        <div>
                            <?php $bgcolor="w3-black"; include( locate_template( 'template-parts/button-promotion.php' ) ); ?>
                            <?php $bgcolor="w3-red"; include( locate_template( 'template-parts/button-competition.php' ) ); ?>
                        </div>
                        <!-- END LOGIN SECTION -->
                    </div>
                </div>
            </div>
	</div><!-- #primary -->
</div>
<div class="w3-row">
    <?php get_footer(); ?>
</div>