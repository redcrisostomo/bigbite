<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Socialight
 */
?>
    <footer>
        <!-- BEGIN PRE-FOOTER -->
        <!-- END PRE-FOOTER -->
        <div class="footer w3-theme-yellow padding-top-30">
            <div class="w3-content w3-container">
                <div class="w3-row">
                    <div class="w3-col m4">
                        <ul class="list-unstyled w3-text-theme-indigo-dark">
                            <li><a href="tel:<?php echo str_replace( array( '-' , ' ', '(', ')' ), '', get_option( 'contact_number' ) );?>" class="w3-hover-text-black"><i class="fa fa-phone"></i><span><?php echo get_option( 'contact_number' ); ?></span></a></li>
                            <li><a href="mailto:<?php bloginfo( 'admin_email' );?>" class=" w3-hover-text-black"><i class="fa fa-envelope-o"></i><span><?php bloginfo( 'admin_email' ); ?></span></a></li>
                        </ul>
                    </div>
                    <div class="w3-col m4 w3-center">
                        2017 © MrDonutPH. All Rights Reserved.
                        <br/>
                        Whitequeen Theme by <a href="http://kixcellent.com/kixtheme" class="w3-hover-text-theme-l1">CMV Themes</a>
                        <div class="w3-opacity w3-padding-8"><img src="<?php echo get_template_directory_uri();?>/assets/global/img/logo.jpg" height="66px"/></div>
                    </div>
                    <div class="w3-col m4 w3-center">
                        
                        <div class="socicon">
                            <?php 
                                $json = file_get_contents( get_template_directory()."/social-media-accounts.json" );
                                $social_media = json_decode( $json, true );
                            ?>
                            <?php foreach( $social_media['accounts'] as $account ) : ?>
                                <?php if( get_option( $account['id'] ) ) : ?>
                            <a class="w3-large w3-text-theme-l5 social-media-hover-text-<?php echo strtolower( $account['title'] );?> w3-hover-shadow no-border" data-original-title="<?php echo strtolower( $account['title'] ); ?>" href="<?php echo get_option( $account['id'] ); ?>" target="_blank"> <i class="fa fa-<?php echo strtolower( $account['title'] );?>"></i></a>
                                <?php endif; /*if( get_option( $account['id'] ) )*/?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="w3-row w3-center">
                </div>
            </div>
        </div>
        <div class="w3-theme-yellow-d1 w3-padding-2"></div>
    </footer>
    <!-- END FOOTER -->

    <!-- Load javascripts at bottom, this will reduce page load time -->
    <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/plugins/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>      
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/js/back-to-top.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/js/kixtheme-owl-carousel.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/global/js/custom.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
<?php wp_footer(); ?>

</body>
</html>
