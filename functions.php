<?php
/**
 * Makina functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Chef's Delight
 */

if ( ! function_exists( 'kixtheme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function kixtheme_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Makina, use a find and replace
	 * to change 'kixtheme' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'kixtheme', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'kixtheme' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'kixtheme_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'kixtheme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function kixtheme_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'kixtheme_content_width', 640 );
}
add_action( 'after_setup_theme', 'kixtheme_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function kixtheme_widgets_init() {
        $widgets = array('sidebar-1'=>'Sidebar 1', 'sidebar-2'=>'Sidebar 2', 'main-top-1'=>'Main Top 1', 'main-top-2'=>'Main Top 2','main-top-3'=>'Main Top 3', 'main-middle-1'=>'Main Middle 1', 'main-middle-2'=>'Main Middle 2', 'main-middle-3'=>'Main Middle 3', 'main-bottom-1'=>'Main Bottom 1', 'main-bottom-2'=>'Main Bottom 2', 'main-bottom-3'=>'Main Bottom 3', 'footer-1'=>'Footer 1');
        foreach($widgets as $id => $name){
            register_sidebar( array(
            'name'          => esc_html__( $name, 'kixtheme' ),
            'id'            => $id,
            'description'   => esc_html__( 'Add widgets here.', 'kixtheme' ),
            'before_widget' => '<section id="%1$s" class="widget %2$s">',
            'after_widget'  => '</section>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
            ));
        }
}
add_action( 'widgets_init', 'kixtheme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function kixtheme_scripts() {
	wp_enqueue_style( 'kixtheme-style', get_stylesheet_uri() );
        
	wp_enqueue_script( 'kixtheme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'kixtheme-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'kixtheme_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load Settings.
 */
require get_template_directory() . '/inc/settings.php';
/**
 * Load Bulletin.
 */
require get_template_directory() . '/inc/bulletin.php';
/**
 * Load Fronpage Banner.
 */
require get_template_directory() . '/inc/frontpage-banner.php';

/**
 * Load Fronpage Pages.
 */
require get_template_directory() . '/inc/frontpage-pages.php';

/**
 * Load Theme Options file.
 */
//require get_template_directory() . '/inc/theme-options.php';

/**
 * Load Post Slider Widget file.
 */
//require get_template_directory() . '/inc/kixtheme-post-slider-widget.php';
