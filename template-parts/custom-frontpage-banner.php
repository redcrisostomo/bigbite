<?php
/**
 * Frontpage Banner by KixTheme
 * 
 * @link https://developer.wordpress.org/themes/template-files-section/post-template-files/
 *
 * @package Hexad
 * @subpackage Frontpage Banner
 * @author Frederick S. Crisostomo
 */
?>

<div id="frontpage-banner" class="frontpage-banner">
    <?php
    $bg_args = array (
    'post_type'              => 'frontpage_banner',
    'post_status'            => 'publish',
    'pagination'             => false,
    'order'                  => 'ASC',
    'posts_per_page'         => '-1',
    'orderby'                => 'menu_order'
        );
    $frontpage_banner = new WP_Query( $bg_args );
    $autoplay = 0;
    if( $frontpage_banner->found_posts > 1 ){
        $autoplay = 1;
    }
    ?>
            <div class="owl-carousel kixtheme-frontpage-banner-carousel" data-column="1" data-auto-play="<?php echo $autoplay; ?>">
                <?php while ( $frontpage_banner->have_posts() ) : $frontpage_banner->the_post(); ?>
                    <div class="w3-col m12 frontpage-banner-item bg-image" style="background-image:url('<?php the_post_thumbnail_url();?>')">
                        <header class="w3-content w3-container w3-padding-48">
                            <?php if ( ! get_post_meta( get_the_ID(), 'wp_frontpage_banner_hide_title', true ) ) : ?>
                            <h1 class="section-header w3-text-theme-red-d1">
                                <span><?php the_title();?></span>
                            </h1>
                            <h1 class="section-header w3-text-theme-yellow">
                                <span class="w3-border-top w3-border-theme-yellow"><?php echo get_post_meta( get_the_ID(), 'wp_frontpage_banner_title2', true ) ?></span>
                            </h1>
                            <?php endif; ?>
                        </header>
                        <article class="w3-content w3-container w3-padding-16">
                            <?php echo get_post_meta( get_the_ID(), 'wp_frontpage_banner_desc', true ) ?>
                        </article>
                    </div>
                    <?php endwhile; ?>
                    <?php	wp_reset_postdata(); ?>
           </div>
</div>