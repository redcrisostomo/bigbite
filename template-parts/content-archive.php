<?php
/**
 * Template part for displaying archives.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Bigbite
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class("blog_posts"); ?> class="margin-bottom-20">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <div class="blog-item-img">
                    <!-- BEGIN THUMBNAIL CAROUSEL--> 
                    <div class="thumbnail ">
                        <?php the_post_thumbnail('single-post-thumb');?>
                    </div>
                    <!-- END CAROUSEL -->             
                </div>
            </div>
            <div class="col-md-8 col-sm-8">
		<?php
		if ( is_single() ) :
			the_title( '<h1>', '</h1>' );
		else :
			the_title( '<h3><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' );
		endif; ?>
<!--        
<?php
		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php kixtheme_posted_on(); ?>
		</div> .entry-meta 
		<?php
		endif; ?>
                -->
            
	
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'kixtheme' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'kixtheme' ),
				'after'  => '</div>',
			) );
		?>
            </div>
	</div><!-- .entry-content -->
        
<!--	<footer class="entry-footer">
		<?php kixtheme_entry_footer(); ?>
	</footer> .entry-footer -->
</article><!-- #post-## -->
