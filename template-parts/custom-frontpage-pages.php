<?php
    $fpp_args = array (
    'post_type'              => 'frontpage_pages',
    'post_status'            => 'publish',
    'pagination'             => false,
    'order'                  => 'ASC',
    'posts_per_page'         => '-1',
    'orderby'                => 'menu_order'
        );
    $frontpage_pages = new WP_Query( $fpp_args );
    ?>
    <?php while ( $frontpage_pages->have_posts() ) : $frontpage_pages->the_post(); ?>
        <section class="w3-row w3-<?php echo get_post_meta( get_the_ID(), 'wp_frontpage_pages_background_color', true ) ?> w3-padding-24">
            <header class="w3-content w3-container">
                <h1 class="w3-text-<?php echo get_post_meta( get_the_ID(), 'wp_frontpage_pages_title_color', true ) ?> section-header">_<br/><?php the_title();?><br/><?php echo get_post_meta( get_the_ID(), 'wp_frontpage_pages_title2', true ) ?><br/></h1>
            </header>
            <article class="w3-content w3-container w3-text-<?php echo get_post_meta( get_the_ID(), 'wp_frontpage_pages_title_color', true ) ?>">
                <p class="w3-col m6"><?php echo get_post_meta( get_the_ID(), 'wp_frontpage_pages_desc', true ) ?></p>
            </article>
        </section>
        <?php if (class_exists('MultiPostThumbnails')) : $images = 0; ?>
            <?php for($i = 1; $i <= 8; $i++) {
                    if (strlen(MultiPostThumbnails::get_post_thumbnail_url( get_post_type(), 'image-'.$i)) > 0) {
                        $images++;
                    } 
                } ?>
            <?php if($images > 0) : ?>
                <div class="w3-row">
                    <?php for($i = 1; $i <= 8; $i++) : ?>
                        <?php if (strlen(MultiPostThumbnails::get_post_thumbnail_url( get_post_type(), 'image-'.$i)) > 0) : ?>
                        <div class="w3-col m<?php echo round( 12/(get_post_meta( get_the_ID(), 'wp_frontpage_pages_image_per_row', true )));?> overflow-hidden">
                            <img src="<?php echo MultiPostThumbnails::get_post_thumbnail_url( get_post_type(), 'image-'.$i); ?>" class="image-width-100 image-zoom-0"/>
                        </div>
                        <?php endif; ?>
                    <?php endfor; ?>
                </div>
            <?php else : ?>
                <div class="w3-row w3-<?php echo str_replace('d1','d2', get_post_meta( get_the_ID(), 'wp_frontpage_pages_background_color', true ) ) ?> w3-padding-16"></div>
            <?php endif; ?>
        <?php else : ?>
            <div class="w3-row w3-<?php echo str_replace('d1','d2', get_post_meta( get_the_ID(), 'wp_frontpage_pages_background_color', true ) ) ?> w3-padding-16"></div>
        <?php endif; ?>
    <?php endwhile; ?>