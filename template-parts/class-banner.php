<?php
/**
 * Main Widget
 *
 * @author Frederick S. Crisostomo
 */
class Banner {

    public function init() { ?>

<div class="w3-row w3-theme-yellow frontpage-banner padding-bottom-30 bg-image" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/global/img/donut/cake_collection.jpg')">
        <header class="w3-content w3-container w3-padding-48">
            <h1 class="section-header w3-text-theme-red-d1">
                <span>SHARE IN</span>
            </h1>
            <h1 class="section-header w3-text-theme-yellow">
                <span class="w3-border-top w3-border-theme-yellow">OUR SUCCESS</span>
            </h1>
        </header>
        <article class="w3-content w3-container w3-padding-16">
            <span class="w3-text-white w3-large"><a href="#" class="w3-btn btn-large w3-theme-indigo-dark">FRANCHISE NOW</a></span>
        </article>
    </div>
 <?php       
    }
}
