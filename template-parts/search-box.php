<?php
/* 
 * SEARCH BOX
 * This contains the search box
 */
?>

<div class="search-box">
    <div class="input-group">
        <input type="text" class="form-control w3-theme-l5 w3-border-0" />
        <span class="input-group-btn w3-yellow">
            <button class="btn w3-yellow" type="submit"><i class="fa fa-search w3-text-white"></i></button>
        </span>
    </div>
</div>