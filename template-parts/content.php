<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Athletix
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class('w3-col m12'); ?>>
	<header class="entry-header">
            <div class="w3-row-padding">
                <div class="w3-col m12 padding-bottom-20">
                    <span class="bold uppercase w3-xxlarge w3-col m9"><?php the_title();?></span>
                    <div class="w3-right w3-col m3">
                        <span class="bold w3-text-theme-indigo-dark w3-center w3-xxlarge"><?php echo get_the_date('M d');?></span>
                    </div>
                <div class="w3-border w3-border-black w3-col m12"></div>
                </div>
            </div>
            <?php if (has_post_thumbnail() ) : ?>
            <div class="w3-row margin-top-20">
                <div class="w3-col m12 divider-3 w3-blue"></div>
            </div>
            <div class="w3-row">
                <div class="w3-col m12 article-header bg-image" style="background: url( '<?php echo the_post_thumbnail_url();?>' )">
                    
                </div>
            </div>
            <div class="w3-row">
                <div class="w3-col m12 divider-3 w3-yellow"></div>
            </div>
            <?php endif; ?>
	</header><!-- .entry-header -->
	<div class="entry-content margin-bottom-20">
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'kixtheme' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) ); ?>
                        
		    <?php
                        wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'kixtheme' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php kixtheme_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->