<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package kixtheme
 */

get_header(); ?>

    <main id="main" class="w3-row w3-white" role="main">
        <div class="w3-content">
            <div class="w3-container w3-col <?php echo ( ! is_active_sidebar( 'sidebar-1' ) && ! is_active_sidebar( 'sidebar-2' ) ) ? '12' : 'm9'; ?>">
                <?php
                while ( have_posts() ) : the_post();
                        get_template_part( 'template-parts/content', get_post_format() );
                ?>  
                    <div class="w3-row">
                <?php the_post_navigation(); ?>
                    </div>
                    <div class="w3-row">
                <?php
                        // If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) :
                                comments_template();
                        endif;
                        ?>
                    </div>
                <?php
                endwhile; // End of the loop.
                ?>
            </div>
            <div class="w3-container w3-col m3 w3-padding-24">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </main><!-- #main -->

<?php
get_footer();
