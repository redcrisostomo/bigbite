<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Socialight
 */
get_header();
?>
<!--    <section>
        <?php
            get_template_part( 'template-parts/class-banner' );
            Banner::init();
        ?>
    </section>-->
<main class="main w3-display-container" style="margin-top:-10px!important;">
    <?php get_template_part( 'template-parts/custom', 'frontpage-pages' ); ?>
        <section class="w3-row bg-image" style="background-image:url('<?php echo get_template_directory_uri();?>/assets/global/img/donut/store_grayscale.jpg')">
            <div class="w3-col m12 w3-theme-indigo w3-opacity w3-padding-64" style="min-height:300px;">
<!--                <header class="w3-content w3-container w3-opacity w3-opacity-off">
                    <h1 class="section-header">
                        <span class="w3-text-white">_</span><br/><span class="w3-text-white">FRANCHISE</span><br/><span class="w3-text-white">NOW</span><br/>
                    </h1>
                </header>
                <article class="w3-content w3-container">
                    <p class="w3-col m6 w3-text-theme-indigo"><a href="#" class="w3-btn w3-white">Franchise Procedure</a></p>
                </article>-->
            </div>
        </section>
        <div class="w3-row w3-theme-indigo w3-padding-24"></div>
        <?php
            get_template_part( 'template-parts/class-main-widget' );
            Main_Widget::init();
        ?>
    </main>
<?php
get_footer();